FROM mambaorg/micromamba

WORKDIR /app

COPY env.yml /app/env.yml
RUN micromamba create -f env.yml
COPY docs/src/my_module.py /app/my_module.py
COPY pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock
COPY README.md /app/README.md
COPY /docs /app/docs
COPY /test_dir /app/test_dir
RUN micromamba run -n pages poetry install --with dev