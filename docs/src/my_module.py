"""Summary of two values."""


def add(a, b):
    """Sum together two given numbers.

    Args:
        a (int): First number
        b (int): Second number

    Returns:
        (int): Sum of two numbers

    """
    return a + b
